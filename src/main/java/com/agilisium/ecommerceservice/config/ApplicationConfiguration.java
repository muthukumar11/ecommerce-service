package com.agilisium.ecommerceservice.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.transaction.annotation.EnableTransactionManagement;

/**
 * Created by Muthukumar Arumugam
 *
 * Configuration class to enable Jpa Repository with package info
 * and enable transaction management
 */
@Configuration
@EnableJpaRepositories(basePackages = {"com.agilisium.ecommerceservice"})
@EnableTransactionManagement
public class ApplicationConfiguration {
}
