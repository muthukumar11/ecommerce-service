package com.agilisium.ecommerceservice.config;

import com.agilisium.ecommerceservice.user.service.UserDetailServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;

/**
 * Created by Muthukumar Arumugam
 *
 * SecurityConfig class to enable authentication for endPoints
 *
 * We can secure specific URLs of our application and make them accessible by users of a specific Role only.
 *
 * userDetailService is used to retrieve the user authentication and authorization information from database
 */
@EnableWebSecurity
@Configuration
public class SecurityConfig extends WebSecurityConfigurerAdapter {

    @Autowired
    UserDetailServiceImpl userDetailService;

    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        auth.userDetailsService(userDetailService);
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.authorizeRequests()
                .antMatchers("**/login", "**/registration", "**/get/all/active").permitAll()
                .antMatchers("/getconfigdetails/**").hasRole("ADMIN")
                .antMatchers("/getproductdetails").hasAnyRole("USER", "ADMIN")
                .and().formLogin()
                .and().logout().permitAll().logoutSuccessUrl("/user/login")
                .and().csrf().disable();
    }
}
