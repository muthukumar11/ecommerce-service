package com.agilisium.ecommerceservice.utility;

import org.springframework.stereotype.Component;

import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.util.Date;

/**
 * Created by Muthukumar Arumugam
 *
 * DateTime related APIs
 */
@Component
public class DateTimeUtility {

    // to get current datetime (system date time)
    public static Timestamp getCurrentDateTime() {
        return Timestamp.valueOf(LocalDateTime.now());
    }

    // to convert timeStamp to given dateTime format
    public static String convertTimestampToString(Timestamp timestamp) {
        if (timestamp != null) {
            Date date = new Date();
            date.setTime(timestamp.getTime());
            return new SimpleDateFormat("dd-MM-yyyy HH:mm:ss").format(date);
        }

        return null;
    }
}
