package com.agilisium.ecommerceservice.utility;

import org.apache.commons.codec.binary.Base64;
import org.springframework.stereotype.Component;

import javax.crypto.Cipher;
import javax.crypto.SecretKey;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;

/**
 * Created by Muthukumar Arumugam
 *
 * This class implements BCrypt algorithm for Password encryption and decryption
 */
@Component
public class BCryptManager {

    /**
     * to encrypt plain text by using key and initVector
     * @param key
     * @param initVector
     * @param value
     * @return
     */
    public static String encryptValue(String key, String initVector, String value) {
        try {
            IvParameterSpec iv = new IvParameterSpec(initVector.getBytes("UTF-8"));
            SecretKey skey = new SecretKeySpec(key.getBytes("UTF-8"), "AES");
            Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5PADDING");
            cipher.init(Cipher.ENCRYPT_MODE, skey, iv);

            byte[] encrypted = cipher.doFinal(value.getBytes("UTF-8"));
            return Base64.encodeBase64String(encrypted);
        } catch (Exception e) {
            throw new RuntimeException(e.getMessage());
        }
    }

    /**
     * to decrypt hash value into plain text by using key and initVector
     * @param key
     * @param initVector
     * @param encrypted
     * @return
     */
    public static String decryptValue(String key, String initVector, String encrypted) {
        try {
            IvParameterSpec iv = new IvParameterSpec(initVector.getBytes("UTF-8"));
            SecretKey skey = new SecretKeySpec(key.getBytes("UTF-8"), "AES");
            Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5PADDING");
            cipher.init(Cipher.DECRYPT_MODE, skey, iv);

            byte[] original = cipher.doFinal(Base64.decodeBase64(encrypted));
            return new String(original, "UTF-8");
        } catch (Exception e) {
            throw new RuntimeException(e.getMessage());
        }
    }
}
