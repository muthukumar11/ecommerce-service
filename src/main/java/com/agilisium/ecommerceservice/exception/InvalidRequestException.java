package com.agilisium.ecommerceservice.exception;

/**
 * Created by Muthukumar Arumugam
 *
 * If invalid request from client to db do operations, we will throw this exception.
 */
public class InvalidRequestException extends RuntimeException {

    public InvalidRequestException(String message) {
        super(message);
    }
}
