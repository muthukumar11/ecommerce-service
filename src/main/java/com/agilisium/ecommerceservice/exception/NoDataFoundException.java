package com.agilisium.ecommerceservice.exception;

/**
 * Created by Muthukumar Arumugam
 *
 * If No data found in database for given request, we will throw this exception..
 */
public class NoDataFoundException extends RuntimeException {

    public NoDataFoundException(String message) {
        super(message);
    }
}
