package com.agilisium.ecommerceservice.exception;

/**
 * Created by Muthukumar Arumugam
 *
 * If data already exist in database, we will throw this exception (Duplicate data validation).
 */
public class DataAlreadyExistException extends RuntimeException {

    public DataAlreadyExistException(String message) {
        super(message);
    }
}
