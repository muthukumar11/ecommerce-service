package com.agilisium.ecommerceservice.exception.handler;

import com.agilisium.ecommerceservice.base.io.BaseResponse;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import java.net.ConnectException;

/**
 * Created by Muthukumar Arumugam
 */
@RestControllerAdvice
public class DataAlreadyExistExceptionHandler {

    @ExceptionHandler(ConnectException.class)
    @ResponseStatus(HttpStatus.CONFLICT)
    public BaseResponse ApplicationException(ConnectException exception) {
        BaseResponse baseResponse = new BaseResponse(
                HttpStatus.CONFLICT.value(), exception.getMessage());

        return baseResponse;
    }
}
