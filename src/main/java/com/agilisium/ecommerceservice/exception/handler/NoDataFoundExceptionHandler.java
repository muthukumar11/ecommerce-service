package com.agilisium.ecommerceservice.exception.handler;

import com.agilisium.ecommerceservice.base.io.BaseResponse;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import java.net.ConnectException;

/**
 * Created by Muthukumar Arumugam
 */
@RestControllerAdvice
public class NoDataFoundExceptionHandler {

    @ExceptionHandler(ConnectException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public BaseResponse ApplicationException(ConnectException exception) {
        BaseResponse baseResponse = new BaseResponse(
                HttpStatus.BAD_REQUEST.value(), exception.getMessage());

        return baseResponse;
    }
}
