package com.agilisium.ecommerceservice.product.controller;

import com.agilisium.ecommerceservice.base.io.BaseResponse;
import com.agilisium.ecommerceservice.base.io.ListResponse;
import com.agilisium.ecommerceservice.product.dto.ProductAddRequestDto;
import com.agilisium.ecommerceservice.product.dto.ProductConfigDetailDto;
import com.agilisium.ecommerceservice.product.dto.ProductDetailDto;
import com.agilisium.ecommerceservice.product.service.ProductService;
import com.wordnik.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

/**
 * Created by Muthukumar Arumugam
 *
 * ProductController which contains product related services
 */
@RestController
public class ProductController {

    private static Logger logger = LoggerFactory.getLogger(ProductController.class);

    @Autowired
    private ProductService productService;

    /**
     * To save products detail into database
     *
     * @param requestDto
     * @return baseResponse (statusCode, message)
     */
    @ApiOperation(value = "Product info save service", response = Iterable.class)
    @ResponseStatus(HttpStatus.OK)
    @RequestMapping(value = "/product/info/save", method = RequestMethod.POST,
            produces = MediaType.APPLICATION_JSON_VALUE)
    public BaseResponse addProductInfo(@RequestBody @Valid ProductAddRequestDto requestDto) {

        logger.info("addProductInfo method invoked.");
        productService.addProductInfo(requestDto);

        BaseResponse baseResponse = new BaseResponse(
                HttpStatus.CREATED.value(), "Product info added successfully");

        return baseResponse;
    }

    /**
     * To retrieve product detail from database
     *
     * Product detail contains list of productId and ProductName
     *
     * @return List of ProductDetailDto and baseResponse(statusCode and message)
     */
    @ApiOperation(value = "Get product details service", response = Iterable.class)
    @ResponseStatus(HttpStatus.OK)
    @RequestMapping(value = "/getproductdetails", method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE)
    public ListResponse<ProductDetailDto> getProductDetails() {

        logger.info("getProductDetails method invoked.");

        List<ProductDetailDto> productDetails = productService.getProductDetails();

        ListResponse<ProductDetailDto> response = new ListResponse<>();
        response.setResult(productDetails);
        response.setRecordCount(productDetails != null
                ? productDetails.size() : 0);
        response.setStatusCode(productDetails != null
                ? HttpStatus.OK.value() : HttpStatus.NOT_FOUND.value());
        response.setMessage((productDetails != null && productDetails.size() > 0)
                ? "Product detail retrieved successfully" : "No data found");

        return response;
    }

    /**
     * To get product configuration
     *
     * This API used to retrieve all details of each product
     *
     * @return List of ProductConfigDetailDto and baseResponse(statusCode and message)
     */
    @ApiOperation(value = "Get product configuration detail service", response = Iterable.class)
    @ResponseStatus(HttpStatus.OK)
    @RequestMapping(value = "/getconfigdetails", method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE)
    public ListResponse<ProductConfigDetailDto> getProductConfigDetails() {

        logger.info("getProductConfigDetails method invoked.");

        List<ProductConfigDetailDto> productConfigDetailDto = productService.getProductConfigDetails();

        ListResponse<ProductConfigDetailDto> response = new ListResponse<>();
        response.setResult(productConfigDetailDto);
        response.setRecordCount(productConfigDetailDto != null
                ? productConfigDetailDto.size() : 0);
        response.setStatusCode(productConfigDetailDto != null
                ? HttpStatus.OK.value() : HttpStatus.NOT_FOUND.value());
        response.setMessage((productConfigDetailDto != null && productConfigDetailDto.size() > 0)
                ? "Product configuration detail retrieved successfully" : "No data found");

        return response;
    }
}
