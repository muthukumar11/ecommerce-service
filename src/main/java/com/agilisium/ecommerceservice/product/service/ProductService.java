package com.agilisium.ecommerceservice.product.service;

import com.agilisium.ecommerceservice.product.dto.ProductAddRequestDto;
import com.agilisium.ecommerceservice.product.dto.ProductConfigDetailDto;
import com.agilisium.ecommerceservice.product.dto.ProductDetailDto;

import java.util.List;

/**
 * Created by Muthukumar Arumugam
 *
 * Interface for product related service declaration
 */
public interface ProductService {

    void addProductInfo(ProductAddRequestDto requestDto);

    List<ProductDetailDto> getProductDetails();

    List<ProductConfigDetailDto> getProductConfigDetails();
}
