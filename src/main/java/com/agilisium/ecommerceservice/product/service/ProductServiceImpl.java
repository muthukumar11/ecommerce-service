package com.agilisium.ecommerceservice.product.service;

import com.agilisium.ecommerceservice.exception.DataAlreadyExistException;
import com.agilisium.ecommerceservice.exception.NoDataFoundException;
import com.agilisium.ecommerceservice.product.dto.ProductAddRequestDto;
import com.agilisium.ecommerceservice.product.dto.ProductConfigDetailDto;
import com.agilisium.ecommerceservice.product.dto.ProductConfigRequestDto;
import com.agilisium.ecommerceservice.product.dto.ProductDetailDto;
import com.agilisium.ecommerceservice.product.entity.ProductInfo;
import com.agilisium.ecommerceservice.product.repository.ProductInfoRepository;
import com.agilisium.ecommerceservice.utility.DateTimeUtility;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

/**
 * Created by Muthukumar Arumugam
 *
 * Product related services
 */
@Service
public class ProductServiceImpl implements ProductService {

    private static Logger logger = LoggerFactory.getLogger(ProductServiceImpl.class);

    @Autowired
    private ProductInfoRepository productInfoRepository;

    /**
     * To save product info into database
     *
     * @param requestDto
     */
    @Override
    public void addProductInfo(ProductAddRequestDto requestDto) {

        logger.info("addProductInfo method invoked");

        // validate duplicate productId
        ProductInfo productInfoByProductId = getProductInfoByProductId(requestDto.getProductId());

        if (productInfoByProductId != null) {
            throw new DataAlreadyExistException("ProductId already exist for another product.");
        }

        ProductInfo productInfo = new ProductInfo();
        productInfo.setProductId(requestDto.getProductId());
        productInfo.setProductName(requestDto.getProductName());
        productInfo.setDescription(requestDto.getDescription());
        productInfo.setProductPrice(requestDto.getProductPrice());
        productInfo.setCreatedBy(requestDto.getLoggedInId());
        productInfo.setCreatedAt(DateTimeUtility.getCurrentDateTime());
        productInfo.setUpdatedBy(requestDto.getLoggedInId());
        productInfo.setUpdatedAt(DateTimeUtility.getCurrentDateTime());
        productInfo.setStatus(true);

        productInfoRepository.save(productInfo);
    }

    /**
     * To get product detail from database
     *
     * @return List of productDetailDto
     */
    @Override
    public List<ProductDetailDto> getProductDetails() {

        List<ProductInfo> productInfoList = productInfoRepository.findByStatus(true);

        if (productInfoList == null && productInfoList.size() > 0) {
            throw new NoDataFoundException("No products found");
        }

        return convertProductInfoToProductDetailDtos(productInfoList);
    }

    /**
     * To get product config details from database
     *
     * @return List of ProductConfigDetailDto
     */
    @Override
    public List<ProductConfigDetailDto> getProductConfigDetails() {

        List<ProductInfo> productInfoList = productInfoRepository.findByStatus(true);

        if (productInfoList == null) {
            throw new NoDataFoundException("Product not found for given productId");
        }

        return convertProductInfoToConfigDto(productInfoList);
    }


    //private methods

    // to get productInfo by productId
    private ProductInfo getProductInfoByProductId(String productId) {

        Optional<ProductInfo> productInfoObj = productInfoRepository.findByProductIdIgnoreCaseAndStatus(
                productId, true);

        if (productInfoObj.isPresent()) {
            return productInfoObj.get();
        }

        return null;
    }

    // convert List of productInfo into list of productDetailDto
    private List<ProductDetailDto> convertProductInfoToProductDetailDtos(List<ProductInfo> productInfoList) {

        List<ProductDetailDto> productDetailDtos = new ArrayList<>();

        for (ProductInfo productInfo : productInfoList) {
            ProductDetailDto productDetailDto = new ProductDetailDto();
            productDetailDto.setProductId(productInfo.getProductId());
            productDetailDto.setProductName(productInfo.getProductName());

            productDetailDtos.add(productDetailDto);
        }

        return productDetailDtos;
    }

    // convert List of productInfo into list of productConfigDetailDto
    private List<ProductConfigDetailDto> convertProductInfoToConfigDto(List<ProductInfo> productInfoList) {

        List<ProductConfigDetailDto> productConfigDetailDtos = new ArrayList<>();

        for (ProductInfo productInfo : productInfoList) {
            ProductConfigDetailDto productConfigDetailDto = new ProductConfigDetailDto();
            productConfigDetailDto.setId(productInfo.getId());
            productConfigDetailDto.setProductId(productInfo.getProductId());
            productConfigDetailDto.setProductName(productInfo.getProductName());
            productConfigDetailDto.setDescription(productInfo.getDescription());
            productConfigDetailDto.setProductPrice(productInfo.getProductPrice());
            productConfigDetailDto.setCreatedAt(DateTimeUtility.convertTimestampToString(productInfo.getCreatedAt()));

            productConfigDetailDtos.add(productConfigDetailDto);
        }

        return productConfigDetailDtos;
    }
}
