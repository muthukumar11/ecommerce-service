package com.agilisium.ecommerceservice.product.dto;

import javax.validation.constraints.NotNull;

/**
 * Created by Muthukumar Arumugam
 */
public class ProductAddRequestDto {

    @NotNull(message = "ProductId can not be empty")
    private String productId;

    @NotNull(message = "ProductName can not be empty")
    private String productName;

    @NotNull(message = "Description can not be empty")
    private String description;

    @NotNull(message = "Product price id can not be empty")
    private Long productPrice;

    @NotNull(message = "LoggedInId can not be empty")
    private String loggedInId;

    public String getProductId() {
        return productId;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Long getProductPrice() {
        return productPrice;
    }

    public void setProductPrice(Long productPrice) {
        this.productPrice = productPrice;
    }

    public String getLoggedInId() {
        return loggedInId;
    }

    public void setLoggedInId(String loggedInId) {
        this.loggedInId = loggedInId;
    }
}
