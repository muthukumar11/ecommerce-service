package com.agilisium.ecommerceservice.product.dto;

/**
 * Created by Muthukumar Arumugam
 */
public class ProductConfigRequestDto {

    private String productId;

    public String getProductId() {
        return productId;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }
}
