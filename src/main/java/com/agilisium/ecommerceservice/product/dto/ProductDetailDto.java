package com.agilisium.ecommerceservice.product.dto;

/**
 * Created by Muthukumar Arumugam
 */
public class ProductDetailDto {

    private String productId;

    private String productName;

    public String getProductId() {
        return productId;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }
}
