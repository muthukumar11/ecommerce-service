package com.agilisium.ecommerceservice.product.repository;

import com.agilisium.ecommerceservice.product.entity.ProductInfo;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

/**
 * Created by Muthukumar Arumugam
 *
 * Repository for product_info
 */
@Repository
public interface ProductInfoRepository extends JpaRepository<ProductInfo, Long> {

    Optional<ProductInfo> findByProductIdIgnoreCaseAndStatus(String productId, Boolean status);

    List<ProductInfo> findByStatus(Boolean status);
}
