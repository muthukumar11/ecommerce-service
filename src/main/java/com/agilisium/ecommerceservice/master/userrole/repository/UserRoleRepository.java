package com.agilisium.ecommerceservice.master.userrole.repository;

import com.agilisium.ecommerceservice.master.userrole.entity.UserRole;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by Muthukumar Arumugam
 *
 * Repository for user_role table
 */
@Repository
public interface UserRoleRepository extends JpaRepository<UserRole, Long> {

    List<UserRole> findByStatus(Boolean status);
}
