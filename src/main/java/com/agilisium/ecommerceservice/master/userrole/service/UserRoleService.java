package com.agilisium.ecommerceservice.master.userrole.service;

import com.agilisium.ecommerceservice.master.userrole.dto.UserRoleDetailDto;

import java.util.List;

/**
 * Created by Muthukumar Arumugam
 *
 * interface for userRole related service declaration
 */
public interface UserRoleService {

    List<UserRoleDetailDto> getAllActiveUserRole();
}
