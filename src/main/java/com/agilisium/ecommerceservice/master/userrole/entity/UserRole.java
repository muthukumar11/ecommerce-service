package com.agilisium.ecommerceservice.master.userrole.entity;

import com.agilisium.ecommerceservice.base.entity.BaseEntity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * Created by Muthukumar Arumugam
 *
 * UserRole entity for user_role table
 */
@Entity
@Table(name = "user_role")
public class UserRole extends BaseEntity {

    @Column(name = "name", nullable = false)
    private String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
