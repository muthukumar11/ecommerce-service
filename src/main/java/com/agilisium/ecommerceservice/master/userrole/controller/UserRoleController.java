package com.agilisium.ecommerceservice.master.userrole.controller;

import com.agilisium.ecommerceservice.base.io.BaseResponse;
import com.agilisium.ecommerceservice.master.userrole.dto.UserRoleDetailDto;
import com.agilisium.ecommerceservice.master.userrole.service.UserRoleService;
import com.wordnik.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * Created by Muthukumar Arumugam
 *
 * UserRoleController
 *
 * User Role is the master data
 */
@RestController
@RequestMapping("/userrole")
public class UserRoleController {

    private static Logger logger = LoggerFactory.getLogger(UserRoleController.class);

    @Autowired
    private UserRoleService userRoleService;

    /**
     *
     * To get all active userRole data from database
     *
     * @return List of UserRoleDetailDto
     */
    @ApiOperation(value = "User registration service", response = Iterable.class)
    @ResponseStatus(HttpStatus.OK)
    @RequestMapping(value = "/get/all/active", method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<UserRoleDetailDto> getAllActiveUserRole() {

        logger.info("getAllActiveUserRole method invoked.");

        List<UserRoleDetailDto> allActiveUserRole = userRoleService.getAllActiveUserRole();

        return new ResponseEntity(allActiveUserRole, HttpStatus.FOUND);
    }
}
