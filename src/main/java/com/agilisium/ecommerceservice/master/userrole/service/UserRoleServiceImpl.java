package com.agilisium.ecommerceservice.master.userrole.service;

import com.agilisium.ecommerceservice.master.userrole.dto.UserRoleDetailDto;
import com.agilisium.ecommerceservice.master.userrole.entity.UserRole;
import com.agilisium.ecommerceservice.master.userrole.repository.UserRoleRepository;
import com.agilisium.ecommerceservice.user.service.UserServiceImpl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Muthukumar Arumugam
 *
 * UserRole service implementation class
 */
@Service
public class UserRoleServiceImpl implements UserRoleService {

    private static Logger logger = LoggerFactory.getLogger(UserServiceImpl.class);

    @Autowired
    private UserRoleRepository userRoleRepository;

    /**
     * Get all active user_role data from database
     *
     * @return List of UserRoleDetailDto
     */
    @Override
    public List<UserRoleDetailDto> getAllActiveUserRole() {

        logger.info("getAllActiveUserRole method invoked");

        List<UserRole> userRoles = userRoleRepository.findByStatus(true);

        return convertUserRoleToDtos(userRoles);
    }


    // private method

    private List<UserRoleDetailDto> convertUserRoleToDtos(List<UserRole> userRoles) {

        List<UserRoleDetailDto> userRoleDetailDtos = new ArrayList<>();

        for (UserRole userRole : userRoles) {
            UserRoleDetailDto userRoleDetailDto = new UserRoleDetailDto();
            userRoleDetailDto.setId(userRole.getId());
            userRoleDetailDto.setName(userRole.getName());
            userRoleDetailDto.setStatus(userRole.getStatus());

            userRoleDetailDtos.add(userRoleDetailDto);
        }

        return userRoleDetailDtos;
    }
}
