package com.agilisium.ecommerceservice.user.service;

import com.agilisium.ecommerceservice.user.dto.UserDetailDto;
import com.agilisium.ecommerceservice.user.dto.UserLoginAuthDto;
import com.agilisium.ecommerceservice.user.dto.UserLoginRequestDto;
import com.agilisium.ecommerceservice.user.dto.UserRegistrationRequestDto;

/**
 * Created by Muthukumar Arumugam
 *
 * Interface to declare user related services
 */
public interface UserService {

    void userRegistration(UserRegistrationRequestDto requestDto);

    UserDetailDto userLogin(UserLoginRequestDto requestDto);

    UserLoginAuthDto userLoginAuthService(String userId);
}
