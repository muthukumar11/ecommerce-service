package com.agilisium.ecommerceservice.user.service;

import com.agilisium.ecommerceservice.exception.DataAlreadyExistException;
import com.agilisium.ecommerceservice.exception.NoDataFoundException;
import com.agilisium.ecommerceservice.user.dto.UserDetailDto;
import com.agilisium.ecommerceservice.user.dto.UserLoginAuthDto;
import com.agilisium.ecommerceservice.user.dto.UserLoginRequestDto;
import com.agilisium.ecommerceservice.user.dto.UserRegistrationRequestDto;
import com.agilisium.ecommerceservice.user.entity.User;
import com.agilisium.ecommerceservice.user.repository.UserRepository;
import com.agilisium.ecommerceservice.utility.BCryptManager;
import com.agilisium.ecommerceservice.utility.DateTimeUtility;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.util.Optional;

/**
 * Created by Muthukumar Arumugam
 *
 * Implemented user related services
 */
@Service
public class UserServiceImpl implements UserService {

    private static Logger logger = LoggerFactory.getLogger(UserServiceImpl.class);

    @Value("${crypy.key}")
    private String crypyKey;

    @Value("${crypy.initvector}")
    private String cryptInitVector;

    @Autowired
    private UserRepository userRepository;

    /**
     * To add new user into database
     *
     * @param requestDto
     */
    @Override
    public void userRegistration(UserRegistrationRequestDto requestDto) {

        User userByUserId = getUserByUserId(requestDto.getUserId());

        if (userByUserId != null) {
            throw new DataAlreadyExistException("UserId already exists for another user");
        }

        User userByContactNumber = getUserByContactNumber(requestDto.getContactNumber());

        if (userByContactNumber != null) {
            throw new DataAlreadyExistException("ContactNumber already exists for another user");
        }

        User user = new User();
        user.setUserId(requestDto.getUserId());
        user.setFirstName(requestDto.getFirstName());
        user.setLastName(requestDto.getLastName());
        user.setCountryCode(requestDto.getCountryCode());
        user.setContactNumber(requestDto.getContactNumber());

        // create password hashcode
        String password = BCryptManager.encryptValue(crypyKey, cryptInitVector, requestDto.getPassword());

        user.setPassword(password);

        user.setUserRoleId(requestDto.getUserRoleId());
        user.setCreatedBy(requestDto.getUserId());
        user.setCreatedAt(DateTimeUtility.getCurrentDateTime());
        user.setUpdatedBy(requestDto.getUserId());
        user.setUpdatedAt(DateTimeUtility.getCurrentDateTime());
        user.setStatus(true);

        userRepository.save(user);
    }

    /**
     * to user login
     *
     * @param requestDto
     * @return UserDetailDto
     */
    @Override
    public UserDetailDto userLogin(UserLoginRequestDto requestDto) {

        User userByUserId = getUserByUserId(requestDto.getUserId());

        if (userByUserId == null) {
            throw new NoDataFoundException("Invalid userId");
        }

        String password = BCryptManager.decryptValue(crypyKey, cryptInitVector, userByUserId.getPassword());

        if (password.equals(requestDto.getPassword())) {
            return convertUserToDto(userByUserId);
        } else {
            throw new DataAlreadyExistException("Invalid password");
        }
    }

    /**
     * User login service which is return user detail including password
     *
     * Don't allow to UI implementation (Not for UI implusinementation)
     * (contain confidential information for security authentication and authorization purpose)
     * @param userId
     * @return
     */
    @Override
    public UserLoginAuthDto userLoginAuthService(String userId) {

        User userByUserId = getUserByUserId(userId);

        if (userByUserId == null) {
            return null;
        }

        String password = BCryptManager.decryptValue(crypyKey, cryptInitVector, userByUserId.getPassword());

        UserLoginAuthDto userLoginAuthDto = new UserLoginAuthDto();
        userLoginAuthDto.setUserId(userByUserId.getUserId());
        userLoginAuthDto.setPassword(password);

        if (userByUserId.getUserRole() != null) {
            System.out.println("UserRole: " + userByUserId.getUserRole());
            userLoginAuthDto.setUserRole(userByUserId.getUserRole().getName());
        }

        return userLoginAuthDto;
    }


    // private methods

    // to get user info by userId
    private User getUserByUserId(String userId) {

        Optional<User> userObj = userRepository.findByUserIdAndStatus(userId, true);

        if (userObj.isPresent()) {
            return userObj.get();
        }

        return null;
    }

    // to get user info by contactNumber
    private User getUserByContactNumber(String contactNumber) {

        Optional<User> userObj = userRepository.findByContactNumberAndStatus(contactNumber, true);

        if (userObj.isPresent()) {
            return userObj.get();
        }

        return null;
    }

    // to convert user into UserDetailDto
    private UserDetailDto convertUserToDto(User user) {
        UserDetailDto userDetailDto = new UserDetailDto();
        userDetailDto.setId(user.getId());
        userDetailDto.setUserId(user.getUserId());
        userDetailDto.setFirstName(user.getFirstName());
        userDetailDto.setLastName(user.getLastName());
        userDetailDto.setCountryCode(user.getCountryCode());
        userDetailDto.setContactNumber(user.getContactNumber());
        userDetailDto.setUserRoleId(user.getUserRoleId());

        if (user.getUserRole() != null) {
            userDetailDto.setUserRoleName(user.getUserRole().getName());
        }

        return userDetailDto;
    }
}
