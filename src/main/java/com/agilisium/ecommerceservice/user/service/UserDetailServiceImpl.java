package com.agilisium.ecommerceservice.user.service;

import com.agilisium.ecommerceservice.exception.NoDataFoundException;
import com.agilisium.ecommerceservice.user.dto.UserLoginAuthDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.User.UserBuilder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

/**
 * Created by Muthukumar Arumugam
 *
 * UserDetailsService is a core interface in Spring Security framework,
 * which is used to retrieve the user’s authentication and authorization information
 *
 * Get user detail by userId and do password validation
 */
@Service
public class UserDetailServiceImpl implements UserDetailsService {

    @Autowired
    private UserService userService;

    /**
     * get user detail from database using userId and do password validation (if required)
     *
     * set required data into UserBuilder, then return UserDetails for user authentication and authorization
     * if no data found for given userId, then throw NoDataFoundException with proper message
     *
     * @param userId
     * @return UserDetails
     * @throws UsernameNotFoundException
     */
    @Override
    public UserDetails loadUserByUsername(String userId) throws UsernameNotFoundException {

        UserBuilder userBuilder = null;

        UserLoginAuthDto userByUserId = userService.userLoginAuthService(userId);


        if (userByUserId != null) {
            userBuilder = org.springframework.security.core.userdetails.User.withUsername(userId);
            userBuilder.password(userByUserId.getPassword());
            userBuilder.roles(userByUserId.getUserRole());
        } else {
            throw new NoDataFoundException("User not found.");
        }

        return userBuilder.build();
    }
}
