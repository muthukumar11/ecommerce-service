package com.agilisium.ecommerceservice.user.entity;

import com.agilisium.ecommerceservice.base.entity.BaseEntity;
import com.agilisium.ecommerceservice.master.userrole.entity.UserRole;

import javax.persistence.*;

/**
 * Created by Muthukumar Arumugam
 *
 * User entity for user table
 */
@Entity
@Table(name = "user")
public class User extends BaseEntity {

    @Column(name = "user_id", nullable = false)
    private String userId;

    @Column(name = "first_name", nullable = false)
    private String firstName;

    @Column(name = "last_name")
    private String lastName;

    @Column(name = "password", nullable = false)
    private String password;

    @Column(name = "country_code", nullable = false)
    private String countryCode;

    @Column(name = "contact_number", nullable = false)
    private String contactNumber;

    @Column(name = "user_role_id", nullable = false)
    private Long userRoleId;

    @OneToOne
    @JoinColumn(name = "user_role_id", referencedColumnName = "id", insertable = false, updatable = false)
    private UserRole userRole;

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getCountryCode() {
        return countryCode;
    }

    public void setCountryCode(String countryCode) {
        this.countryCode = countryCode;
    }

    public String getContactNumber() {
        return contactNumber;
    }

    public void setContactNumber(String contactNumber) {
        this.contactNumber = contactNumber;
    }

    public Long getUserRoleId() {
        return userRoleId;
    }

    public void setUserRoleId(Long userRoleId) {
        this.userRoleId = userRoleId;
    }

    public UserRole getUserRole() {
        return userRole;
    }

    public void setUserRole(UserRole userRole) {
        this.userRole = userRole;
    }
}
