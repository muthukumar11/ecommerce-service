package com.agilisium.ecommerceservice.user.dto;

import javax.validation.constraints.NotNull;

/**
 * Created by Muthukumar Arumugam
 */
public class UserLoginRequestDto {

    @NotNull(message = "UserId required")
    private String userId;

    @NotNull(message = "Password required")
    private String password;

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
