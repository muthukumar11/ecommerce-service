package com.agilisium.ecommerceservice.user.dto;

/**
 * Created by Muthukumar Arumugam
 */
public class UserLoginAuthDto {

    private String userId;

    private String password;

    private String userRole;

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getUserRole() {
        return userRole;
    }

    public void setUserRole(String userRole) {
        this.userRole = userRole;
    }
}
