package com.agilisium.ecommerceservice.user.dto;

import javax.validation.constraints.NotNull;

/**
 * Created by Muthukumar Arumugam
 */
public class UserRegistrationRequestDto {

    @NotNull(message = "UserId can not be empty")
    private String userId;

    @NotNull(message = "FirstName can not be empty.")
    private String firstName;

    private String lastName;

    @NotNull(message = "Password can not be empty.")
    private String password;

    @NotNull(message = "CountryCode can not be empty.")
    private String countryCode;

    @NotNull(message = "ContactNumber can not be empty.")
    private String contactNumber;

    @NotNull(message = "UserRoleId can not be empty.")
    private Long userRoleId;

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getCountryCode() {
        return countryCode;
    }

    public void setCountryCode(String countryCode) {
        this.countryCode = countryCode;
    }

    public String getContactNumber() {
        return contactNumber;
    }

    public void setContactNumber(String contactNumber) {
        this.contactNumber = contactNumber;
    }

    public Long getUserRoleId() {
        return userRoleId;
    }

    public void setUserRoleId(Long userRoleId) {
        this.userRoleId = userRoleId;
    }
}
