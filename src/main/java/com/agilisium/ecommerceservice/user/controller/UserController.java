package com.agilisium.ecommerceservice.user.controller;

import com.agilisium.ecommerceservice.base.io.BaseResponse;
import com.agilisium.ecommerceservice.base.io.Response;
import com.agilisium.ecommerceservice.user.dto.UserDetailDto;
import com.agilisium.ecommerceservice.user.dto.UserLoginRequestDto;
import com.agilisium.ecommerceservice.user.dto.UserRegistrationRequestDto;
import com.agilisium.ecommerceservice.user.service.UserService;
import com.wordnik.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

/**
 * Created by Muthukumar Arumugam
 *
 * User related services
 */
@RestController
@RequestMapping("/user")
public class UserController {

    private static Logger logger = LoggerFactory.getLogger(UserController.class);

    @Autowired
    private UserService userService;

    /**
     * To register new user into database
     *
     * @param requestDto
     * @return baseResponse (statusCode and message)
     */
    @ApiOperation(value = "User registration service", response = Iterable.class)
    @ResponseStatus(HttpStatus.OK)
    @RequestMapping(value = "/registration", method = RequestMethod.POST,
            produces = MediaType.APPLICATION_JSON_VALUE)
    public BaseResponse userRegistration(@RequestBody @Valid UserRegistrationRequestDto requestDto) {

        logger.info("userRegistration method invoked.");

        userService.userRegistration(requestDto);

        BaseResponse baseResponse = new BaseResponse(
                HttpStatus.CREATED.value(), "User registered successfully.");

        return baseResponse;
    }

    /**
     * To user login
     *
     * @param requestDto
     * @return userDetailDto
     */
    @ApiOperation(value = "User login service", response = Iterable.class)
    @ResponseStatus(HttpStatus.OK)
    @RequestMapping(value = "/login", method = RequestMethod.POST,
            produces = MediaType.APPLICATION_JSON_VALUE)
    public Response<UserDetailDto> userLogin(@RequestBody @Valid UserLoginRequestDto requestDto) {

        logger.info("userLogin method invoked.");

        UserDetailDto userDetailDto = userService.userLogin(requestDto);

        Response<UserDetailDto> response = new Response<>();
        response.setResult(userDetailDto);

        response.setStatusCode(HttpStatus.OK.value());
        response.setMessage("User login successful");

        return response;
    }
}
