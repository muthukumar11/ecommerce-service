package com.agilisium.ecommerceservice.user.repository;

import com.agilisium.ecommerceservice.user.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

/**
 * Created by Muthukumar Arumugam
 *
 * Repository for user table
 */
@Repository
public interface UserRepository extends JpaRepository<User, Long> {

    Optional<User> findByUserIdAndStatus(String userId, Boolean status);

    Optional<User> findByContactNumberAndStatus(String contactNumber, Boolean status);
}
