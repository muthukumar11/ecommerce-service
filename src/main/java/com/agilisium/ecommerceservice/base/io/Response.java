package com.agilisium.ecommerceservice.base.io;

/**
 * Created by Muthukumar Arumugam
 */
public class Response<T> extends BaseResponse {

    private T result;

    public T getResult() {
        return result;
    }

    public void setResult(T result) {
        this.result = result;
    }
}
