package com.agilisium.ecommerceservice.base.io;

import java.io.Serializable;

/**
 * Created by Muthukumar Arumugam
 *
 * BaseResponse contains statusCode and message fields
 */
public class BaseResponse implements Serializable {

    public BaseResponse() {

    }

    public BaseResponse(int statusCode, String message) {
        this.statusCode = statusCode;
        this.message = message;
    }

    private int statusCode;

    private String message;

    public int getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(int statusCode) {
        this.statusCode = statusCode;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
