package com.agilisium.ecommerceservice.base.io;

import java.util.List;

/**
 * Created by Muthukumar Arumugam
 *
 * ListResponse is a generic class which is used to store list of data and total record count
 *
 * We can set baseResponse value by using getter method
 */
public class ListResponse<T> extends BaseResponse {

    private List<T> result;

    private int recordCount;

    public List<T> getResult() {
        return result;
    }

    public void setResult(List<T> result) {
        this.result = result;
    }

    public int getRecordCount() {
        return recordCount;
    }

    public void setRecordCount(int recordCount) {
        this.recordCount = recordCount;
    }
}
